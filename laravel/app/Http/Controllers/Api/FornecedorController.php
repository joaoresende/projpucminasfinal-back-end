<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Http\Controllers\ApiControllerTrait;

class FornecedorController extends Controller
{
    use ApiControllerTrait;

    protected $model;

    public function __construct(\App\Fornecedor $model)
    {
        $this->model = $model;
    }
}