<?php

namespace App\Http\Controllers\Api;

use \App\Http\Controllers\ApiControllerTrait;
use App\Http\Controllers\Controller;

class ProdutoController extends Controller
{
    use ApiControllerTrait;

    protected $model;

    public function __construct(\App\Produto $model)
    {
        $this->model = $model;
    }
}
