<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtors';
    protected $fillable = [
        'nome_produto',
        'ds_produto',
        'vlr_produto',
        'qtd_produto',
        'path_imagem_produto',
    ];
}
