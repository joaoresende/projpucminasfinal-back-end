<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $table = 'fornecedors';
    protected $fillable = [
        'ds_nome_fantasia',
        'ds_endereco',
        'ds_bairro',
        'ds_cidade',
        'co_uf',
        'ds_telefone',
        'co_cep',
        'co_cnpj',
        'ds_email',
        'ds_inscricao',
        'ds_ramo_atividade'
    ];
}
