<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ds_nome_fantasia', 100);
            $table->string('ds_endereco', 100);
            $table->string('ds_bairro', 100);
            $table->string('ds_cidade', 100);
            $table->string('co_uf', 100);
            $table->string('ds_telefone', 100);
            $table->integer('co_cep');
            $table->string('co_cnpj', 50);
            $table->string('ds_email', 100);
            $table->string('ds_inscricao', 50);
            $table->string('ds_ramo_atividade', 100);
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedors');
    }
}
